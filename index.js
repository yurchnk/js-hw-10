const tabs = document.querySelectorAll('.tabs');
const tabs_title = document.querySelectorAll('.tabs li');
const tabContents = document.querySelectorAll('.tabs-content li');

function showTab(tab, tabIndex) {
  tabContents.forEach(tabContent => {
    tabContent.style.display = 'none';
  });
  tabs_title.forEach(item=>{item.classList.remove('active')});
  tab.classList.add('active');

  tabContents[tabIndex].style.display = 'block';
}

document.querySelectorAll('ul li').forEach((tab, index) => {

    tab.addEventListener('click', () => {
        showTab(tab, index);

    });

 });
